// Soal 1
console.log("Soal No. 1");

function balikString(string) {
    var results = "";
    for (var i=string.length-1; i >= 0; i--){
        results += string[i];
    }
    return results;
}

console.log(balikString("abcde"));
console.log(balikString("rusak"));
console.log(balikString("racecar"));
console.log(balikString("haji"));
console.log(" ");


// Soal 2
console.log("Soal No. 2");

function palindrome(string){
    var results = "";
    for (var i=string.length-1; i>=0; i--){
        results += string[i];
    }

    if (results === string){
        return true;
    } else {
        return false;
    }
}

console.log(palindrome("kasur rusak"));
console.log(palindrome("haji ijah"));
console.log(palindrome("nasabah"));
console.log(palindrome("nababan"));
console.log(palindrome("jakarta"));
console.log(" ");


// Soal 3
console.log("Soal No. 3");

function bandingkan(number, asli){
    if ((number > 0) && (asli > 0) ){
        if (number > asli) {
            return number;
        } else if (asli > number) {
            return asli;
        } else if (number == asli) {
            return -1;
        }
    }else {
        return -1;
    }
}

console.log(bandingkan(10, 15));
console.log(bandingkan(12, 12));
console.log(bandingkan(-1, 10));
console.log(bandingkan(112, 121));
console.log(bandingkan(1));
console.log(bandingkan());
console.log(bandingkan("15","18"));
console.log(" ");


// Soal 4
console.log("Soal 4");

function DescendingTen(input){
    results = [];
    if (input > 0){
        for (var i=input; i>=input-9; i--){
            results.push(i);
        }
    } else {
        results.push(-1);
    }

    return results.toString();
}

console.log(DescendingTen(100));
console.log(DescendingTen(10));
console.log(DescendingTen());