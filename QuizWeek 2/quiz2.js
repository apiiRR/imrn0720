// Soal Class Score

class Score {
    constructor(subject, points, email){
        this._subject = subject;
        this._points = points;
        this._email = email
    }

    average() {
        let total = 0;
        if (this._points.length > 0) {
            for (let i = 0; i < this._points.length; i++){
                total += this._points[i];
            }
            let average = total / this._points.length;
            return average;
        } else {
            return this._points;
        }
    }

}

let skor = new Score ("quiz-1",[10,30,50],"saya@gmail.com");
console.log(skor.average());
console.log(" ");


// Soal View Score
console.log("Soal view Score");

const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
]


const viewScores = (data, subject) => {
    let header = ["email", "subject","points"];
    let output = [];
    for (let i = 0; i < data.length; i++){
        let object = {};
        object[header[0]] = data[i][0];
        object[header[1]] = subject;

        for (let j = 1; j < data[i].length; j++) {
            switch(subject) {
                case "quiz-1":
                    object[header[2]] = data[i][1];
                    break;
                case "quiz-2":
                    object[header[2]] = data[i][2];
                    break;
                case "quiz-3":
                    object[header[2]] = data[i][3];
                    break;
            }
        }
        output[i] = object;
    }
    output.shift();
    console.log(output);
}

viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");
console.log(" ");


// Soal Recap Score
console.log("Soal Recap Score");

const recapScores = (data) => {
    let sum = (value, currVal) => value + currVal;
    
    for (let i = 1; i < data.length; i++) {
        let nilai = [];
            for (let index = 1; index < data[i].length; index++) {
                nilai.push(data[i][index]);
            }
        let average = nilai.reduce(sum) / nilai.length;
        let predikat = (average > 90) ? "honour" 
                    : (average > 80) ? "graduate" 
                    : (average > 70) ? "participant" 
                    : "";      
        console.log(
        i + "." + " Email: " + data[i][0] + "\n" + 
        "Rata-rata: " + average.toFixed(1) + "\n" + 
        "Predikat: " + predikat + "\n" 
        );     
    }   
}
recapScores(data);  
console.log(" ");



