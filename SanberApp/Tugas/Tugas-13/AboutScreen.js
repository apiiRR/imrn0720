import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class AboutScreen extends Component {
    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <Text style={styles.title}>Tentang Saya</Text>
                    <View style={styles.user}>
                        <Icon style={styles.icon} name='user-circle-o' size={200} />
                        <Text style={styles.usertitle}>Rafi Ramadhana</Text>
                        <Text style={styles.subUserTitle}>React Native Developer</Text>
                    </View>
                    <View style={styles.portofolio}>
                        <Text style={styles.portofolioTitle}>Portofolio</Text>
                        <View style={styles.portofolioBorder}></View>
                        <View style={styles.portofolioIcon}>
                            <View style={styles.iconGitlab} >
                                <Icon style={styles.gitIcon} name='gitlab' size={40} />
                                <Text style={styles.textIcon}>@apiiRR</Text>
                            </View>
                            <View style={styles.iconGithub} >
                                <Icon style={styles.gitIcon} name='github' size={40} />
                                <Text style={styles.textIcon}>@apiiRR</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.hubungi}>
                        <Text style={styles.portofolioTitle}>Hubungi Saya</Text>
                        <View style={styles.portofolioBorder}></View>
                        <View style={styles.hubungiIcon}>
                            <View style={styles.iconFacebook} >
                                <Icon style={styles.socIcon} name='facebook-official' size={40} />
                                <Text style={styles.textIcon}>Rafi Ramadhana</Text>
                            </View>
                            <View style={styles.iconInstagram} >
                                <Icon style={styles.socIcon} name='instagram' size={40} />
                                <Text style={styles.textIcon}>@Rafii_Ramadhanaa</Text>
                            </View>
                            <View style={styles.iconTwitter} >
                                <Icon style={styles.socIcon} name='twitter' size={40} />
                                <Text style={styles.textIcon}>Rafi_Ramadhana</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },  
    title: {
        fontSize: 36,
        lineHeight: 42,
        fontWeight: 'bold',
        color: '#003366',
        textAlign: 'center',
        marginTop: 64,
        marginBottom: 12
    },
    user: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 16
    },
    icon: {
        color: 'darkgray',
        marginTop: 12,
        marginBottom: 24
    },
    usertitle: {
        color: '#003366',
        fontSize: 24,
        lineHeight: 28,
        fontWeight: 'bold',
    },
    subUserTitle: {
        color : '#3EC6FF',
        fontSize: 16,
        lineHeight: 19
    },
    portofolio: {
        width: 359,
        height: 140,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: '#EFEFEF',
        borderRadius: 16
    },
    portofolioTitle: {
        fontSize: 18,
        lineHeight: 21,
        fontWeight: 'normal',
        textAlign: 'left',
        paddingTop: 5,
        paddingLeft: 8,
        color: '#003366'
    },
    portofolioBorder : {
        borderBottomWidth: 1,
        color: '#003366',
        marginTop: 8,
        marginLeft: 8,
        marginRight: 8
    },
    portofolioIcon: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
    },
    iconGitlab: {
        marginRight: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconGithub: {
        marginLeft: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textIcon: {
        fontSize: 16,
        lineHeight: 19,
        fontWeight: 'bold',
        color: '#003366',
        marginTop: 11,
        marginBottom: 17
    },
    gitIcon: {
        color: '#3EC6FF'
    },
    hubungi: {
        width: 359,
        height: 251,
        marginTop: 9,
        marginBottom: 64,
        marginLeft: 8,
        marginRight: 8,
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
    },
    socIcon: {
        color: '#3EC6FF',
        marginRight: 19
    },
    iconFacebook: {
        flexDirection: 'row',
        marginLeft: 90,
        marginTop: 19
    },
    iconInstagram: {
        flexDirection: 'row',
        marginLeft: 90,
        marginTop: 29
    },
    iconTwitter: {
        flexDirection: 'row',
        marginLeft: 90,
        marginTop: 29,
        marginBottom: 22
    }
})