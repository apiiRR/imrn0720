import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput} from 'react-native';


export default class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container} >
                <View style={styles.turboTron}>
                    <Image source={require('./Images/logo.png')} style={{width: 375, height: 102}} />
                </View>
                <Text style={styles.formTitle}>Login</Text>
                <View style={styles.form} >
                    <Text style={styles.lable}>Username / Email</Text>
                    <TextInput style={styles.textInput} />
                    <Text style={styles.lable}>Password</Text>
                    <TextInput style={styles.textInput} />
                </View>
                <TouchableOpacity style={styles.masuk}>
                    <Text style={{fontSize: 24, lineHeight: 28, marginTop: 6, marginBottom: 6, marginLeft: 34, marginRight: 34, color: 'white'}}>Masuk</Text>
                </TouchableOpacity>
                <Text style={{fontSize: 24, lineHeight: 28, textAlign: 'center', color: '#3EC6FF', marginTop: 16, marginBottom: 16}}>atau</Text>
                <TouchableOpacity style={styles.daftar}>
                    <Text style={{fontSize: 24, lineHeight: 28, marginTop: 6, marginBottom: 6, marginLeft: 28, marginRight: 28, color: 'white'}}>Daftar ?</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        fontFamily: 'Roboto',
        alignItems: 'center'
    },
    turboTron: {
        marginTop: 63
    },
    formTitle: {
        fontSize: 24,
        color: '#003366',
        textAlign: 'center',
        marginTop: 70
    },
    form : {
        marginTop: 40,
        marginRight: 40,
        marginLeft: 40,
        marginBottom: 16
    },
    lable: {
        color: '#003366',
        fontSize: 16,
        lineHeight: 19,
        marginBottom: 5
    },
    textInput: {
        height: 48, 
        backgroundColor: 'white', 
        borderWidth: 1, 
        borderColor: '#003366',
        marginBottom: 16,
        width: 294
    },
    masuk : {
        width: 140,
        height: 40,
        alignItems: 'center',
        backgroundColor: '#3EC6FF',
        borderRadius: 16
    },
    daftar: {
        width: 140,
        height: 40,
        alignItems: 'center',
        backgroundColor: '#003366',
        borderRadius: 16,
        marginBottom: 179
    }
    
});