import React from "react";
import {NavigationContainer, DrawerActions} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import RegisterScreen from './RegisterScreen';
import {AddScreen} from './AddScreen';
import {ProjectScreen} from './ProjectScreen';
import SkillScreen from './SkillScreen';

const Tabs = createBottomTabNavigator();
const TabScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="Skill" component={SkillScreen} />
        <Tabs.Screen name="Project" component={ProjectScreen} />
        <Tabs.Screen name="Add" component={AddScreen} />
    </Tabs.Navigator>
)

const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
    <AboutStack.Navigator>
        <AboutStack.Screen name='About' component={AboutScreen} />
    </AboutStack.Navigator>
)

const Home = ({route}) => (
    <Drawer.Navigator>
            <Drawer.Screen name='Home' component={TabScreen} />
            <Drawer.Screen name='About' component={AboutStackScreen} />
    </Drawer.Navigator>
)

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default () => (
    <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Register" component={RegisterScreen} />
        {/* <Drawer.Navigator>
            <Drawer.Screen name='Home' component={TabScreen} />
            <Drawer.Screen name='About' component={AboutStackScreen} />
        </Drawer.Navigator> */}
        </Stack.Navigator>
    </NavigationContainer>
)