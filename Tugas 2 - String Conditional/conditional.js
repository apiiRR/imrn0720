// if - else

var nama = "Jane";
var peran = "Penyihir";

if (nama != " "  && peran == " ") {
    console.log("Halo "+ nama +", Pilih peranmu untuk memulai game!");
} else if (nama && peran == " "){
    console.log("Nama harus diisi!");
} else if (peran == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Penyihir "+ nama + ", kamu dapat meilihat siapa yang menjadi warewolf!");
} else if (peran == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama);
    console.log("Halo Guard "+ nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
} else if (peran == "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, "+ nama);
    console.log("Halo Werewolf "+ nama + ", Kamu akan memakan mangsa setiap malam!");
}
console.log(" ");



// Switch Case

var tanggal = 31;
var bulan = 12;
var tahun = 2100;

switch (tanggal) {
    case 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31:
        tanggal = tanggal
        break;
    default:
        tanggal = "Tidak Valid!"
        break;
}

switch (bulan) {
    case 1:
        bulan = "Januari"
        break;
    case 2:
        bulan = "Februari"
        break;
    case 3:
        bulan = "Maret"
        break;
    case 4:
        bulan = "April"
        break;
    case 5:
        bulan = "Mei"
        break;
    case 6:
        bulan = "Juni"
        break;
    case 7:
        bulan = "Juli"
        break;
    case 8:
        bulan = "Agustus"
        break;
    case 9:
        bulan = "September"
        break;
    case 10:
        bulan = "Oktober"
        break;
    case 11:
        bulan = "November"
        break;
    case 12:
        bulan = "Desember"
        break;
    default:
        bulan = "Tidak Valid!"
        break;
}

switch (true) {
    case tahun >= 1900 && tahun <= 2200:
        tahun = tahun
        break;
    default:
        tahun = "Tidak Valid!"
        break;
}
console.log(tanggal +" "+ bulan +" "+ tahun);