// No 1 Looping While
console.log("No. 1 Looping While")
console.log("LOOPING PERTAMA")
var pertama = 2
while (pertama <= 20) {
    console.log(pertama + ' - I love coding')
    pertama = pertama + 2;
}

console.log("LOOPING KEDUA")
var kedua = 20
while (kedua >= 2) {
    console.log(kedua + ' - I love coding')
    kedua = kedua - 2;
}
console.log(" ")


// No. 2 Looping menggunakan for

console.log("Looping menggunakan for")
console.log("OUTPUT")
for(var i = 1; i <= 20; i++) {
    if(i % 3 == 0 && i % 1 == 0 ){
        console.log(i + " - I Love Coding")
    } else if (i % 1 == 0){
        console.log(i + " - Santai")
    } else if (i % 2 == 0) {
        console.log(i + " - Berkualitas")
    }
}
console.log(" ");


// No. 3 Membuat Persegi Panjang

console.log("No. 3 Membuat Persegi Panjang")
for(var i = 1; i <= 4; i++){
     
     var str = '';
     
     for(var j = 1; j <= 8; j++){
       str += '#';
     }
     
     console.log(str);
}
console.log(" ")


// No. 4 Membuat Tangga

console.log("No. 4 Membuat Tangga")
for (var i = 1; i <= 7; i++){
     
     var str = " ";

     for(var j = 1; j <= i; j++) {
          str += "*";
     }

     console.log(str);
}
console.log(" ")


// No. 5 Membuat Papan Catur

console.log("No. 5 Membuat Papan Catur")
var board = " ";
for (var y=0; y<8; y++){
     for (var x=0; x < 8; x++){
          if ((x+y) % 2 == 0){
               board += " ";
          } else {
               board += "#";
          }
     }
     board += "\n";
}
console.log(board)

