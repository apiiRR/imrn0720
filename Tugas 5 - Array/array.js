//  Soal No. 1 (Range)
console.log("Soal No. 1 (Range) ")

function range(startNum, finishNum) {
    var results = [];
    if (startNum < finishNum) {
        for (var i = startNum; i<= finishNum; i++ ){
            results.push(i);
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--){
            results.push(i);
        }
    } else {
        results = -1
    }

    return results;
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());
console.log(" ");


// Soal No.2 (Range with Step)
console.log("Soal No.2 (Range with Step)");

function rangeWithStep(startNum, finishNum, step) {
    var results = [];
    if (startNum < finishNum) {
        for (var i = startNum; i<= finishNum; i=i+step){
            results.push(i);
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i=i-step){
            results.push(i);
        }
    }

    return results;
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));
console.log(" ");


// Soal No. 3 (Sum of Range)
console.log("Soal No. 3 (Sum of Range)");

function sum(startNum, finishNum, step) {
    var results = [];
    var total = 0;
    if (startNum < finishNum) {
        if (step > 0){
            for (var i = startNum; i<= finishNum; i = i+step){
                results.push(i);
                total = results.reduce((hasil, value) => hasil + value, 0);
            }
        }else {
            for (var i = startNum; i<= finishNum; i++){
                results.push(i);
                total = results.reduce((hasil, value) => hasil + value, 0);
            }
        }
        
    } else if (startNum > finishNum) {
        if (step > 0){
            for (var i = startNum; i >= finishNum; i=i-step){
                results.push(i);
                total = results.reduce((hasil, value) => hasil + value, 0);
            }
        } else {
            for (var i = startNum; i>= finishNum; i--){
                results.push(i);
                total = results.reduce((hasil, value) => hasil + value, 0);
            }
        }
    } else {
        if (startNum > 0){
            results.push(startNum);
            total = results.reduce((hasil, value) => hasil + value, 0);
        } else {
            return total;
        }
    }

    return total;
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());
console.log(" ");


// Soal No. 4 (Array Multidimensi)
console.log("Soal No. 4 (Array Multidimensi)");

function dataHandling(input){
    for (var i = 0, l1 = input.length; i < l1; i++){
        console.log("Nomor ID : "+input[i][0]);
        console.log("Nama Lengkap : "+input[i][1]);
        console.log("TTL : "+input[i][2]+" "+input[i][3]);
        console.log("Hobi : "+input[i][4]);
        console.log(" ");

    }
}
console.log(" ");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

dataHandling(input);


// Soal No.5 (Balik Kata)
console.log("Soal No.5 (Balik Kata)");

function balikKata(str){
    var results = "";

    for (var i= str.length -1; i >= 0; i--){
        results += str[i];
    }

    return results;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("Sanbercode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));
console.log(" ");


// Soal No. 6 (Metode Array)
console.log("Soal No. 6 (Metode Array)");

function dataHandling2(input){
    input.splice(1, 1, "Roman Alamsyah Elsharawwy");
    input.splice(2, 1, "Provinsi Bandar Lampung");
    input.splice(4, 0, "Pria");
    input.splice(5, 1, "SMA Internasional Metro");
    console.log(input);

    tanggal = input[3];
    tanggalSplit = tanggal.split('/').join('-');
    var bulan = parseInt(tanggalSplit.substr(3, 2));
    switch(bulan){
        case 01:
            bulan = "Januari"
            break;
        case 02:
            bulan = "Februari"
            break;
        case 03:
            bulan = "Maret"
            break;
        case 04:
            bulan = "April"
            break;
        case 05:
            bulan = "Mei"
            break;
        case 06:
            bulan = "Juni"
            break;
        case 07:
            bulan = "Juli"
            break;
        case 08:
            bulan = "Agustus"
            break;
        case 09:
            bulan = "September"
            break;
        case 10:
            bulan = "Oktober"
            break;
        case 11:
            bulan = "November"
            break;
        case 12:
            bulan = "Desember"
            break;
        default:
            break;
    }
    console.log(bulan)

    tanggalDesc = tanggalSplit.split('-').join(",");
    hasil = tanggalDesc.split( /\s*,\s*/)
    console.log(hasil.sort(function(a, b){return b-a}));

    console.log(tanggalSplit);

    var x = input[1].toString();
    console.log(x.slice(0, 15));
}

var input2 = ["0001","Roman Alamsyah","Bandar Lampung","21/05/1989","Membaca"]
dataHandling2(input2);