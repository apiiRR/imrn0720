// Soal No. 1 (Array to Object)
console.log("Soal No. 1 (Array to Object)");

function arrayToObject(array) {
    var now = new Date()
    var thisYear = now.getFullYear();
    let obj = {}
    if (array.length <= 0){
        return console.log("");
    } else {
            for (let index = 0; index < array.length; index++) {

            let key = [index+1] + "." + " " + array[index][0] + ' ' + array[index][1]
            obj[key] = {
                firstName: array[index][0],
                lastName: array[index][1],
                gender: array[index][2],
                age: array[index][3] == null || array[index][3] > thisYear? "Invalid Birth Year" 
                : thisYear - (array[index][3])
            }
        }
    }
    console.log(obj);

}

var people = [["Bruce","Banner","male","1975"],["Natasha", "Romanoff", "female"]];
arrayToObject(people);

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2);

arrayToObject([]);
console.log(" ");



// Soal No. 2 (Shopping Time)
console.log("Soal No.2 (Shopping Time)");

function shoppingTime(memberId, money) {
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    } else {
        var object = {};
        var moneyChange = money;
        var list = [];

        var sepatuStacattu = "Sepatu Stacattu";
        var bajuZoro = "Baju Zoro";
        var bajuHn = "Baju H&N";
        var sweaterUniklooh = "Sweater Uniklooh";
        var casingHandphone = "Casing Handphone";

        var check = 0;

        for (var i = 0; moneyChange >= 50000 && check == 0; i++){
            if (moneyChange >= 1500000){
                list.push(sepatuStacattu);
                moneyChange -= 1500000;
            } else if (moneyChange >= 500000){
                list.push(bajuZoro);
                moneyChange -= 500000;
            } else if (moneyChange >= 250000){
                list.push(bajuHn);
                moneyChange -= 250000;
            } else if (moneyChange >= 175000){
                list.push(sweaterUniklooh);
                moneyChange -= 175000;
            } else if (moneyChange >= 50000){
                if (list.length != 0){
                    for (var j = 0; j <= list.length-1; j++){
                        if (list[j] == casingHandphone){
                            check += 1;
                        }
                    }
                    if (check == 0) {
                        list.push(casingHandphone);
                        moneyChange -= 50000;
                    }
                } else {
                    list.push(casingHandphone);
                    moneyChange -= 50000;
                }
            }
        }

        object.memberId = memberId;
        object.money = money;
        object.list = list;
        object.moneyChange = moneyChange;


        return object;
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());
console.log(" ");


// Soal No. 3 (Naik Angkot)
console.log("Soal No. 3 (Naik Angkot)");

function naikAngkot(arrPenumpang) {
    var rute = ['A','B','C','D','E','F'];
    var results = [];

    if (arrPenumpang.length <= 0){
        return [];
    }

    for (var i = 0; i < arrPenumpang.length; i++){
        var object = {};

        var asal = arrPenumpang[i][1];
        var tujuan = arrPenumpang[i][2];

        var indexAsal;
        var indexTujuan;

        for (var j = 0; j < rute.length; j++){
            if (rute[j] == asal) {
                indexAsal = j;
            } else if (rute[j] == tujuan) {
                indexTujuan = j;
            }
        }

        var bayar = (indexTujuan - indexAsal) * 2000

        object.penumpang = arrPenumpang[i][0];
        object.naikDari = asal;
        object.tujuan = tujuan;
        object.bayar = bayar;

        results.push(object)
    }

    return results;
}

console.log(naikAngkot([['Dimitri','B','F'],['Icha','A','B']]));
console.log(naikAngkot([]));