// Soal No.1 Animal Class
console.log("Soal No. 1 Animal Class");

// release 0
console.log("Release 0");

class Animal{
    constructor(name){
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }

    get name(){
        return this._name;
    }

    get legs(){
        return this._legs;
    }

    get cold_blooded(){
        return this._cold_blooded;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);
console.log("");

// Release 1
console.log("Release 1");

class Frog extends Animal{
    constructor(name){
        super(name);
    }

    jump() {
        console.log("hop hop");
    }
}

class Ape extends Animal {
    constructor(name){
        super(name);
    }

    yell() {
        console.log("Auooo");
    }
}

var sungokong = new Ape("Kera sakti");
sungokong.yell();

var kodok = new Frog("buduk");
kodok.jump();
console.log("");

// Soal No. 2 Function to Class
console.log("Soal No. 2 Function to Class");

class Clock {
    constructor({template}){
        this._template = template;
    }

    render(){
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) haours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop(){
        clearInterval(this.timer);
    }

    start(){
        this.render();
        this.timer = setInterval(() => this.render(), 1000)
    }
}

var clock = new Clock ({template: 'h:m:s'});
clock.start();