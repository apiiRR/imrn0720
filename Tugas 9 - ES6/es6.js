// Soal Nomer 1
console.log("Soal No. 1");

const golden = () => {
    console.log("this is golden!!");
}

golden();
console.log("");


// Soal Nomer 2
console.log("Soal No. 2");

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`);
            return
        }
    }
}

newFunction("William","Imoh").fullName();
console.log("");


// Soal Nomer 3
console.log("Soal No. 3");

const newObject = {
    firstName : "Harry",
    lastName : "Potter Holt",
    destination : "Hogwarts React Conf",
    occupation : "Deve-wizard Avocado",
    spell : "Vimulus Renderus!!!"
};

const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation);
console.log("");


// Soal Nomer 4
console.log("Soal No. 4");

const west = ["Will","Chris","Sam","holly"];
const east = ["Gill","Brian","Noel","Maggie"];
const combined = [...west, ...east];

console.log(combined);
console.log("");


// Soal Nomer 5
console.log("Soal No. 5");

const planet = "earth";
const view = "glass";
let before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;

console.log(before)